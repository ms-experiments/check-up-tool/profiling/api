import Koa from 'koa';
import KoaLogger from 'koa-logger';
import cors from '@koa/cors';
import bodyParser from 'koa-bodyparser';
import json from 'koa-json';

import { router } from './router';

const port = process.env.PORT || 8000;
const app: Koa = new Koa();

app.use(KoaLogger());
app.use(cors());
app.use(bodyParser());
app.use(json());

app.use(router.routes()).use(router.allowedMethods());

app.listen(port, () => console.log('server running'));
