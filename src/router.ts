import Router from 'koa-router';
import { Context } from 'koa';
import axios from 'axios';

export const router = new Router();

router.get('/profiling', async (ctx: Context) => {
  let { sex, age }: any = ctx.query;

  sex = sex.toLowerCase();
  age = Number.parseInt(age);

  const {
    data: { diseases },
  } = await axios.get('http://diseasesapi:8000/diseases');

  const jointDiseases = [];
  for (const disease of diseases) {
    const {
      data: { actions },
    } = await axios.get(
      `http://actionsapi:8000/diseases/${disease.uuid}/actions`
    );

    jointDiseases.push({
      ...disease,
      actions,
    });
  }

  ctx.body = {
    diseases: jointDiseases.filter((disease: any): boolean =>
      disease.actions.some((action: any): boolean =>
        action.targets.some(
          (t: any): boolean =>
            t.sex == sex && t.minAge <= age && t.maxAge >= age
        )
      )
    ),
  };
});
